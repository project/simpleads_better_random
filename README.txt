Simpleads Better Random Ordering
================================
Maintainer: Tim Kamanin (http://drupal.org/user/49393), http://timonweb.com


The Problem:
------------

Random ordering in Simpleads sucks a little bit: if you have field_ad_status in your simplead node, it will respect its
value, BUT, only after it made a query. So, if you have inactive ads in your group, it will query an ad at first and then
check if it is active and if it is not, SimpleAds won't show the ad and you will lose an impression.

The Solution:
-------------

This small module fixes that situation and uses EFQ to get a random active ad for the current ad group, so you'll never
miss an impression, unless you have all ads inactive.

Installation and Usage:
-----------------------

Just install it and enable. In your ad group block select a 'Better Random' under Ads order select and save. It will work!